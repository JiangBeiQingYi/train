# train
1. 12306购票
1. service 为调用12306 数据解析业务
1. swing 为 使用java swing 开发的客户端工具 
1. web 后台使用spring boot  前端使用bootstrap 实现简单12306购票系统


#java swing 实现的12306购票客户端
![登录页面](http://git.oschina.net/uploads/images/2017/0104/140435_bd110d73_140698.png "登录页面")

![验证码页面](http://git.oschina.net/uploads/images/2017/0104/140512_dcc633f5_140698.png "验证码页面")

![查询列车提交订单页面](http://git.oschina.net/uploads/images/2017/0104/140626_ef3b8d6f_140698.png "查询列车提交订单页面")

![订单管理页面](http://git.oschina.net/uploads/images/2017/0104/140747_b935bfe0_140698.png "订单管理页面")